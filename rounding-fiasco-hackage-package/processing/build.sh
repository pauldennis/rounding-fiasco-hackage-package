set -e
echo "$1"

pushd RoundingFiasco
  cabal v2-test

  cabal v2-sdist --list-only
  cabal v2-sdist --output-directory "../$1"

  cabal v2-haddock --builddir="../$1" --haddock-for-hackage --enable-doc
popd
