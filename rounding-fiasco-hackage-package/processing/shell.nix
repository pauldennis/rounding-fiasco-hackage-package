{ sources ? import ./nix/sources.nix
, pkgs ? import sources.nixpkgs {}
, glibcLocales ? pkgs.glibcLocales

, ghc ? pkgs.haskell.compiler.native-bignum.ghc981

, nix ? pkgs.nix
, eza ? pkgs.eza
, cacert ? pkgs.cacert
, git ? pkgs.git
, cabal-install ? pkgs.cabal-install
, rsync ? pkgs.rsync
}:

pkgs.mkShell {
  buildInputs = [
    eza
    nix
    cacert

    git
    cabal-install
    ghc
    rsync
  ];

  shellHook = ''
    echo this is a nix shell; alias l="ls -al"
  '';

  # excruciating unicode and whatnot
  LOCALE_ARCHIVE="${pkgs.glibcLocales}/lib/locale/locale-archive";
  LANG="en_GB.UTF8";

  MY_ENVIRONMENT_VARIABLE = "world";
}
