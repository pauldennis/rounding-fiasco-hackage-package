# Revision history for RoundingFiasco

## 0.1.0.0 -- 2024-04-08

* floats: single, double
* rounding variants: ceil, floor, truncate
* operators: +-*/√
* conversions: promotion, demotion
* helpers: arithmic_signum, sign_bit, successor, predecessor
