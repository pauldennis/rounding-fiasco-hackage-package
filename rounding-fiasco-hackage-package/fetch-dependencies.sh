set -e

git submodule update --init --recursive

pushd input-channels/niv-sources

  git status
  git fetch
  git reset --hard origin/main
  git checkout origin/main

popd
